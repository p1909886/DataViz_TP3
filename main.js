var width = 700,
    height = 580;

var svg = d3.select( "body" )    // creation du svg
    .append( "svg" )             // dans le dom
    .attr( "width", width )
    .attr( "height", height );


var tooltip = d3.select('body')
    .append('div')
    .attr('class', 'hidden tooltip');

var projection = d3.geoMercator()
    .center([2.454071, 46.279229])  // Centered on France (longitude, latitude)
    .scale(2000)  // Adjust the scale as needed
    .translate([width / 2, height / 2]);  // Adjust the translation based on your map size

var path = d3.geoPath()          // mapping des donnees
    .projection(projection);    // spatiales a la proj.

var color = d3
    .scaleQuantize()
    .range(["#edf8e9", "#bae4b3", "#74c476", "#31a354", "#006d2c"]);

var jourChoisi = "2021-09-12" // pour demarrer on code en dur un jour a afficher


function addDaysToDate(daysToAdd) {
    const result = new Date("2021-06-01");
    result.setDate(result.getDate() + daysToAdd);
    return result.toISOString().split('T')[0]; // Format as 'YYYY-MM-DD'
}


function drawMap(currendDay, json) {

    // on dessine
    svg.selectAll("path")
    .data(json.features)
    .join("path")
    .attr("d", path)
    .style("fill", function(d) {
            var value = d.properties.value[currendDay];

            if(value) {
                return color(value);
            } else {
                return "#ccc";
            }        
    })
    .on('mousemove', function(e, d) {
        // on recupere la position de la souris, 
        // e est l'object event d
        var mousePosition = [e.x, e.y];
        
        
        // on affiche le toolip
        tooltip.classed('hidden', false)
          // on positionne le tooltip en fonction 
          // de la position de la souris 
            .attr('style', 'left:' + (mousePosition[0] + 15) +
              'px; top:' + (mousePosition[1] - 35) + 'px'+";position:absolute;color: #222;background-color: #c0bbbb;padding: .5em;text-shadow: #f5f5f5 0 1px 0;border-radius: 2px;opacity: 0.8;position: absolute;")
            // on recupere le nom de l'etat 
            .html(d.properties.nom + ':<br/>' + d.properties.value[currendDay]+" hospit.");

        // add border line when hover
        d3.select(this)
            .style('stroke', 'black')
            .style('stroke-width', 1);
    })
    .on('mouseout', function() {
        // on cache le toolip
        tooltip.classed('hidden', true);
        tooltip.attr('style', '');
        tooltip.html("");

        // remove border line
        d3.select(this)
            .style('stroke', 'none');
    });
}


d3.csv("covid.csv").then(function(data) {


    var cleanData = data.filter(function(d) { 
        return d.sexe == 0 ;
    });

    // d3.extent use return [d3.min, d3.max]
    let rangeHosp = d3.extent(cleanData, d => parseInt(d.hosp));

    // set color range to the min and max number of hosp
    color.domain(rangeHosp);

    d3.json("dep_france.geojson").then(function(json) {
        

        //On parcours les departements du GeoJSON un par un
        for (var j = 0; j < json.features.length; j++) {
			var departement = json.features[j].properties.code;

            var depChoisi = cleanData.filter(function(row) {
                return row.dep == departement;
            });
            
            json.features[j].properties.value = depChoisi.map(d => d.hosp);
	    }


        d3.select("#slider").on("input", function() {
            drawMap(+this.value, json);
            d3.select("#day").text(addDaysToDate(+this.value));
        });

        drawMap(0, json);
    });
});








